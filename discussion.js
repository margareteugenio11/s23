db.users.insertOne(
	{
		"firstName": "Tony",
		"lastName": "Stark",
		"username": "iAmIronMan",
		"email": "iloveyou3000@gmail.com",
		"password": "starkIndustries",
		"isAdmin": true
	}
)

// db. collections.insertMany()
	// allows us to insert or create two or more documents.

	db.users.insertMany([
	{
		"firstName": "Pepper",
		"lastName": "Potts",
		"username": "resueArmor",
		"email": "pepper@mail.com",
		"password": "whereIsTonyAgain",
		"isAdmin": false

	},
	{
		"firstName": "Steve",
		"lastName": "Rogers",
		"username": "theCaptain",
		"email": "steve@mail.com",
		"password": "iCanLiftMjolnirToo",
		"isAdmin": false
	},
	{
		"firstName": "Loki",
		"lastName": "Odinson",
		"username": "godOfMischief",
		"email": "lokia@mail.com",
		"password": "iAmReallyLoki",
		"isAdmin": false
	},

])